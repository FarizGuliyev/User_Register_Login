package lesson4.user.service;

import lesson4.user.dto.UserLoginDto;
import lesson4.user.dto.UserPasswordResetDto;
import lesson4.user.dto.UserRegisterDto;
import lesson4.user.entity.User;
import lesson4.user.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserService implements IUserService {

    private final UserRepository userRepository;
    private final ModelMapper mapper;

    public UserService(UserRepository userRepository, ModelMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    @Override
    public void registerUser(UserRegisterDto UserRegisterDto) {
        User user = mapper.map(UserRegisterDto, User.class);
        userRepository.save(user);
    }

    @Override
    public boolean loginUser(UserLoginDto userLoginDto) {
        User user = userRepository.findByUserName(userLoginDto.getUserName());
        if (user.getPassword().equals(userLoginDto.getPassword())) {
            return true;
        } else return false;
    }

    @Override
    public void resetPassword(UserPasswordResetDto passwordResetDto) {
        User user = userRepository.findByMail(passwordResetDto.getMail());
        user.setPassword(passwordResetDto.getPassword());
        userRepository.save(user);
    }

    @Override
    public User getOneUserByUserName(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public User getOneUserByEmail(String email) {
        return userRepository.findByMail(email);
    }

    @Override
    public boolean validatePassword(UserRegisterDto UserRegisterDto) {
        if (UserRegisterDto.getPassword().equals(UserRegisterDto.getConfirmPassword())) {
            return true;
        } else return false;
    }

    @Override
    public boolean validateVerificationEmail(UserPasswordResetDto passwordResetDto) {
        User user = userRepository.findByMail(passwordResetDto.getMail());
        if (user.getVerificationEmail().equals(passwordResetDto.getVerificationEmail())) {
            return true;
        } else return false;
    }

    @Override
    public boolean validateVerificationCode(UserPasswordResetDto passwordResetDto) {
        User user = userRepository.findByMail(passwordResetDto.getMail());
        if (user.getVerificationCode().equals(passwordResetDto.getVerificationCode())) {
            return true;
        } else return false;
    }
}
