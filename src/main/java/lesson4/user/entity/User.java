package lesson4.user.entity;

import lombok.Data;

import javax.persistence.*;
import java.net.PasswordAuthentication;

@Entity
@Data
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String userName;
    private String  password;
    private String mail;
    private String verificationEmail;
    private String verificationCode;


}
