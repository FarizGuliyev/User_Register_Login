FROM alpine:3.11.2
Run apk add --no-cache openjdk11
Copy build/libs/user-0.0.1-SNAPSHOT.jar /user-app/
Cmd ["java","-jar","user-app/user-0.0.1-SNAPSHOT.jar"]